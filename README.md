## Getting started

When pull code from GitLab repository, tou need to have node version which is shown in .nvmrc file (current version is [16.12.0]). To check your node versions hit nvm ls then nvm use v16.12.0 if have it installed, if not install it.
After having right node version installed and selected, head to projects location and type npm install command in terminal in order to install all node modules and packages.
Head to .env_sample file to see all environment variables used in this project, and create .env file with correct variables and their values in order to start project.
When everything is set up, type npm run local in terminal to see if application is up and running. If not, follow error messages to fix errors.

## DB and ORM

Database is PosgreSQL a relational database.I have created database using pgadmin4 and I have connected to it using connection configuration which can be found in `config` folder as `config.service.ts` file.
I am using typeorm as ORM that lets me to query and manipulate data from a database using an object-oriented paradigm. As project is Typescript based, this ORM was good choice.

## Project architecture

Project is made up of Nest framework having Express as default HTTP server framework. Project's architecture is made up of module folders as well as with configuration and helpers folders.

There are several modules:

1. User
2. Contacts
3. Auth
4. Email
5. Email confirmation
   Each module consists of
   -controllers together with routes files -> Responsible for receiving requests and routing them, and pass data to corresponding service to handle database and business logic
   -services -> Handling database and business logic, fetching data, processing data, & other operations.
   And may consist of:
   -entities -> Creat Schemas, modeling application data
   -dto's -> Defining how data will be transfered withing processes

All of those functions is connected together with file named `module` to organize the application's structure

Beside this folders, there are:

config -> Configure database connection
migration -> Keep info about how to define tables inside database
scripts -> Functions for handling application's settings such as seeding the database, type orm configuration

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Swagger

You can see by adding `docs` to the base url.
