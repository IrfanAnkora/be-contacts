import { Controller, Request, Post, UseGuards } from '@nestjs/common';
import { LocalAuthGuard } from './auth/local-auth.guard';
import { AuthService } from './auth/auth.service';
import { EmailConfirmationGuard } from './emailConfirmation/emailConfirmation.guard';
import RequestWithUser from './auth/requestWithUser';

@Controller()
export class AppController {
  constructor(private authService: AuthService) {}

  @UseGuards(EmailConfirmationGuard)
  @UseGuards(LocalAuthGuard)
  @Post('user/login')
  async login(@Request() req: RequestWithUser) {
    return this.authService.login(req.user);
  }
}
