import {
  Controller,
  Post,
  Body,
  Get,
  Param,
  ParseIntPipe,
  Patch,
  Delete,
  UseGuards,
  Request,
} from '@nestjs/common';
import { ContactService } from './contact.service';
import { Contact } from './contact.entity';
import { CreateContactDto } from './contact.dto';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import RequestWithUser from '../auth/requestWithUser';

@Controller('contacts')
export class ContactController {
  constructor(private readonly contactService: ContactService) {}

  @UseGuards(JwtAuthGuard)
  @Get(':contactId')
  async getOneContact(
    @Param('contactId', ParseIntPipe) contactId: number,
    @Request() req: RequestWithUser,
  ): Promise<Contact> {
    const { id } = req.user;
    return await this.contactService.getContact(contactId, id);
  }

  @UseGuards(JwtAuthGuard)
  @Post()
  async createContact(
    @Body() body: CreateContactDto,
    @Request() req: RequestWithUser,
  ): Promise<Contact> {
    const { id } = req.user;
    return await this.contactService.createContact(body, id);
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  async getManyContacts(
    @Request() req: RequestWithUser,
  ): Promise<Contact[] | []> {
    const { id } = req.user;
    return await this.contactService.getManyContacts(id);
  }

  @UseGuards(JwtAuthGuard)
  @Patch(':contactId')
  async updateContact(
    @Body() body: CreateContactDto,
    @Param('contactId', ParseIntPipe) contactId: number,
    @Request() req: RequestWithUser,
  ): Promise<Contact> {
    const { id } = req.user;
    return await this.contactService.updateContact(body, contactId, id);
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':contactId')
  async deleteContact(
    @Param('contactId', ParseIntPipe) contactId: number,
    @Request() req: RequestWithUser,
  ): Promise<void> {
    const { id } = req.user;
    return await this.contactService.deleteContact(contactId, id);
  }
}
