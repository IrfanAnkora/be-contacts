import { IsEmail, IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class CreateContactDto {
  @IsString()
  @IsNotEmpty()
  public firstName: string;

  @IsString()
  public middleName: string;

  @IsString()
  @IsNotEmpty()
  public lastName: string;

  @IsEmail()
  public email: string;

  @IsString()
  public phone: string;

  @IsNumber()
  @IsNotEmpty()
  public user: number;
}
