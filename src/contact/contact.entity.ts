import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  Unique,
} from 'typeorm';
import { User } from '../user/user.entity';

@Entity()
@Unique(['email'])
export class Contact extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'first_name', type: 'varchar', nullable: false, length: 300 })
  firstName: string;

  @Column({ name: 'middle_name', type: 'varchar', nullable: true, length: 300 })
  middleName: string;

  @Column({ name: 'last_name', type: 'varchar', nullable: false, length: 300 })
  lastName: string;

  @Column({ nullable: false, length: 300 })
  email: string;

  @Column({ nullable: true, length: 300 })
  phone: string;

  // @ManyToOne(() => User)
  @ManyToOne((type) => User, (user) => user.id)
  @JoinColumn()
  user: number;

  @CreateDateColumn({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  createdDate: Date;

  @CreateDateColumn({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  updatedDate: Date;

  public static from(dto: Partial<Contact>) {
    const it = new Contact();
    it.id = dto.id;
    it.firstName = dto.firstName;
    it.middleName = dto.middleName;
    it.lastName = dto.lastName;
    it.email = dto.email;
    it.phone = dto.phone;
    it.user = dto.user;
    return it;
  }
}
