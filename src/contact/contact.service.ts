import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, getConnection } from 'typeorm';
import { CreateContactDto } from './contact.dto';
import { Contact } from './contact.entity';

@Injectable()
export class ContactService {
  constructor(
    @InjectRepository(Contact)
    private readonly contactRepository: Repository<Contact>,
  ) {}

  async getContact(id: number, userId: number): Promise<Contact> {
    const contact = await this.contactRepository.findOne({ id, user: userId });
    if (!contact) {
      throw new NotFoundException(
        'There is no contact with that id or it is not in your contact list',
      );
    }
    return contact;
  }

  async createContact(
    body: CreateContactDto,
    userId: number,
  ): Promise<Contact> {
    const contact: Contact = new Contact();

    contact.firstName = body.firstName;
    contact.middleName = body.middleName;
    contact.lastName = body.lastName;
    contact.email = body.email;
    contact.phone = body.phone;
    contact.user = userId;

    return await this.contactRepository.save(contact);
  }

  async getManyContacts(userId: number): Promise<Contact[] | []> {
    return await this.contactRepository.find({ user: userId });
  }

  async updateContact(
    body: CreateContactDto,
    id: number,
    userId: number,
  ): Promise<Contact> {
    await getConnection()
      .createQueryBuilder()
      .update(Contact)
      .set({ ...body })
      .where('id = :id AND userId = :userId', { id, userId })
      .execute();
    return await this.contactRepository.findOne({ id, user: userId });
  }

  async deleteContact(id: number, userId: number): Promise<void> {
    const contact = await this.contactRepository.findOne({ id, user: userId });
    if (!contact) {
      throw new NotFoundException(
        'There is no contact with that id or it is not in your contact list',
      );
    }
    await getConnection()
      .createQueryBuilder()
      .delete()
      .from(Contact)
      .where('id = :id AND userId = :userId', { id, userId })
      .execute();
  }
}
