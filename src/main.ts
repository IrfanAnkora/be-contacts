import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { configService } from './config/config.service';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  const port = 4000;

  if (!configService.isProduction()) {
    const document = SwaggerModule.createDocument(
      app,
      new DocumentBuilder()
        .setTitle('Contacts API')
        .setDescription('My Contacts API')
        .build(),
    );

    SwaggerModule.setup('docs', app, document);
  }

  console.log(`Application is running on a port: ${port}`);
  await app.listen(port);
}

bootstrap();
