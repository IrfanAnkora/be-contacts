// scripts/seed.ts
import * as _ from 'lodash';
import { createConnection, ConnectionOptions } from 'typeorm';
import { configService } from '../config/config.service';
import { UserService } from '../user/user.service';
import { User } from '../user/user.entity';
import { ContactService } from '../contact/contact.service';
import { Contact } from '../contact/contact.entity';

async function run() {
  const seedId = Date.now()
    .toString()
    .split('')
    .reverse()
    .reduce((s, it, x) => (x > 3 ? s : (s += it)), '');

  const opt = {
    ...configService.getTypeOrmConfig(),
    debug: true,
  };

  const connection = await createConnection(opt as ConnectionOptions);
  const userService = new UserService(connection.getRepository(User));
  const contactService = new ContactService(connection.getRepository(Contact));

  const workUsers = _.range(1, 10)
    .map((n) =>
      User.from({
        firstName: `seed${seedId}-${n}`,
        middleName: `seed${seedId}-${n}`,
        lastName: `seed${seedId}-${n}`,
        email: `testuser${n}@gmail.com`,
        password: `seed${seedId}-${n}`,
      }),
    )
    .map((dto) =>
      userService
        .createUser(dto)
        .then((r) => (console.log('done ->', r.firstName), r)),
    );

  const workContacts = _.range(1, 10)
    .map((n) =>
      Contact.from({
        firstName: `seedContact${seedId}-${n}`,
        middleName: `seedContact${seedId}-${n}`,
        lastName: `seedContact${seedId}-${n}`,
        email: `seedContact${n}@gmail.com`,
        phone: `${seedId}-${n}-${seedId}`,
        user: 1,
      }),
    )
    .map((dto) =>
      contactService
        .createContact(dto, dto.user)
        .then((r) => (console.log('done ->', r.firstName), r)),
    );

  return await Promise.all([workUsers, workContacts]);
}

run()
  .then((_) => console.log('...wait for script to exit'))
  .catch((error) => console.error('seed error', error));
