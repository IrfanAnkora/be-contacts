import {
  Body,
  Controller,
  Get,
  Param,
  ParseIntPipe,
  Post,
  ClassSerializerInterceptor,
  UseInterceptors,
} from '@nestjs/common';
import { EmailConfirmationService } from '../emailConfirmation/emailConfirmation.service';
import { CreateUserDto } from './user.dto';
import { User } from './user.entity';
import { UserService } from './user.service';

@Controller('user')
@UseInterceptors(ClassSerializerInterceptor)
export class UserController {
  constructor(
    private readonly userService: UserService,
    private readonly emailConfirmationService: EmailConfirmationService,
  ) {}

  @Get(':id')
  async getUser(@Param('id', ParseIntPipe) id: number): Promise<User> {
    return this.userService.getUser(id);
  }

  @Post()
  async createUser(@Body() body: CreateUserDto): Promise<User> {
    const user = await this.userService.createUser(body);
    await this.emailConfirmationService.sendVerificationLink(body.email);
    return user;
  }
}
