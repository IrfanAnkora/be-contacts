import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  Unique,
} from 'typeorm';
import bcrypt from 'bcrypt';
import { Exclude } from 'class-transformer';

@Entity()
@Unique(['email'])
export class User extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'first_name', type: 'varchar', nullable: false, length: 300 })
  firstName: string;

  @Column({ name: 'middle_name', type: 'varchar', nullable: true, length: 300 })
  middleName: string;

  @Column({ name: 'last_name', type: 'varchar', nullable: false, length: 300 })
  lastName: string;

  @Column({ type: 'varchar', nullable: false, length: 300 })
  email: string;

  @Exclude()
  @Column({ type: 'varchar', nullable: false, length: 300 })
  password: string;

  @Column({ default: false })
  isEmailConfirmed: boolean;

  @CreateDateColumn({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  createdDate: Date;

  @CreateDateColumn({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  updatedDate: Date;

  async validatePassword(password: string): Promise<boolean> {
    const saltRounds = 10;
    const hash = bcrypt.hashSync(password, saltRounds);
    return hash === this.password;
  }

  public static from(dto: Partial<User>) {
    const it = new User();
    it.id = dto.id;
    it.firstName = dto.firstName;
    it.middleName = dto.middleName;
    it.lastName = dto.lastName;
    it.email = dto.email;
    it.password = dto.password;
    return it;
  }
}
