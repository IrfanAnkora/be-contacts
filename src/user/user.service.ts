import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserDto } from './user.dto';
import { User } from './user.entity';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
  ) {}

  async getUser(id: number): Promise<User> {
    const user = this.userRepository.findOne(id);
    if (!user) {
      throw new NotFoundException("User with such id doesn't exist");
    }
    return user;
  }

  async findUserByEmail(email: string): Promise<User | undefined> {
    const user = await this.userRepository.findOne({ email });
    if (!user) {
      throw new NotFoundException('There is no user with that email');
    }
    return user;
  }

  async createUser(body: CreateUserDto): Promise<User> {
    const user: User = new User();

    user.firstName = body.firstName;
    user.middleName = body.middleName;
    user.lastName = body.lastName;
    user.email = body.email;
    const hash = bcrypt.hashSync(body.password, 10);
    user.password = hash;

    const createdUser = await this.userRepository.save(user);
    return createdUser;
  }

  async markEmailAsConfirmed(email: string) {
    return this.userRepository.update(
      { email },
      {
        isEmailConfirmed: true,
      },
    );
  }
}
